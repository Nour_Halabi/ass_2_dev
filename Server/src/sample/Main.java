package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class Main extends Application {
FileHandler fh = new FileHandler();
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));


        //String words = "Hello my name is Nour. \n I like to eat pizza.\n  My friends are nice.";
        //File testFile = new File ("SharedFolder/new.txt");
        // fh.SaveTextToFile(fh.ReadtextFromFile(new File("random.txt")), testFile);
        //System.out.print(fh.ReadtextFromFile(testFile));

        fh.getDirectoryList(new File("SharedFolder"));

        try {
            Server server = new Server(8080);
            server.handleRequests();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void main(String[] args) {
        launch(args);
    }
}
