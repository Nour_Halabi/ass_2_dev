package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nourhalabi on 27/03/16.
 */
public class FileHandler {

    private String file_text;
    public ArrayList<Files> tempFilesList;

    public FileHandler () {
        file_text = "";
        tempFilesList = new ArrayList<>();
    }

    //_____Reads in the file and stores it in a string which gets returned_____//
    public String ReadtextFromFile(File file)throws IOException {

        if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                file_text += scanner.nextLine();
                file_text += "\r\n";
            }
        }

        else {
        System.out.println("File does not exist.");
        }

        Files files = new Files(file.getName(), file_text);
        tempFilesList.add(files);
        return file_text;
    }


    //_____Print text to output file_____//
    public void SaveTextToFile(String File_text, File outputFile)throws IOException {
        System.out.println("Saving text to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            fout.println(File_text);
            fout.close();
        } else {
            System.err.println("Cannot write to output file");

        }
    }

    public String getDirectoryList(File file) {
        String filenames = "";
        if (file.isDirectory()) {

            //retrieve the list of all files in the directory
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {

                //get the names of the files
                filenames += filesInDir[i].getName();
                filenames += ","; //separator is comma

            }
        }

        System.out.println(filenames);
        return filenames;
    }

    public ObservableList<Files> getAllTestFiles() {
        ObservableList<Files> filesList = FXCollections.observableArrayList();

        for(int i = 0; i < tempFilesList.size(); i++) {
            filesList.add(new Files(tempFilesList.get(i).getFilename(), tempFilesList.get(i).getText()));
        }

        return filesList;
    }


}
