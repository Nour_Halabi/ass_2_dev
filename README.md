Assignment #2:
ISHRAF UDDIN 100520689 
NOUR HALABI 100525020
________________________________________________________________________________________________
Running program Steps:
 To download you will need to clone the repository in a folder of your choice:
git clone https://Nour_Halabi@bitbucket.org/Nour_Halabi/ass_2_dev.git 
git clone https://100520689@bitbucket.org/Nour_Halabi/csci2020u_assignment2.git

There are two projects folders included in the repository, one for client and another for server.
 Both projects should be opened separately in their own InteliJ window. It is important to note 
that the Server project should be run before the Client. Runt>main in server, once you see 
"Simple Http Server v1.0: Ready to handle incoming requests." you may run the Client project's main.

what will show up after running the Client's main, a set of tables will show up showing what
is the local folder and the shared folder on the server side. Each project has there own 
folders where files are uploaded and downloaded. You may select a file from the local Folder 
table and press upload. You should be able to see the file has been added to the on shared folders 
table as well as the file has been to the correct folder.
________________________________________________________________________________________________
SECONDARY INSTRUCTIONS:
To make the UI abit more user friendly we went with some tooltips which appear when you hover your 
mouse over eachof the buttons. The tooltips let the user know what they are supposed to select 
before pressing the button. We placing of everything was taken into consideration as well to make 
it more neat and plesant to the eyes.
________________________________________________________________________________________________
ADDITIONAL COMMENTS:
Due to repository issues we were forced to create a new repository but the history can be
viewed for this repository from this link:https://bitbucket.org/Nour_Halabi/csci2020u_assignment2/overview

In addition, we ran into an issue last minute with download button was freezing the client when clicked on.
Due to shortage of time we were not able to fix it, however all functionality is still provided and the 
upload button still works.
