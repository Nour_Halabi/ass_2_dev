package sample;

import java.io.*;
import java.net.*;
import java.util.Vector;

/**
 * Created by nourhalabi on 27/03/16.
 */
public class Server {

    private ServerSocket serverSocket = null;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void handleRequests() throws IOException {
        System.out.println("Simple Http Server v1.0: Ready to handle incoming requests.");

        // repeatedly handle incoming requests
        while(true) {
            Socket socket = serverSocket.accept();
            Thread handlerThread = new Thread(new ClientConnectionHandler(socket));
            handlerThread.start();
        }
    }

    public void runServer()
    {
        int port = 8080;
        try {
            Server server = new Server(8080);
            server.handleRequests();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   /* public static void main(String[] args) {
        int port = 8080;
        try {
            Server server = new Server(8080);
            server.handleRequests();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */
}



