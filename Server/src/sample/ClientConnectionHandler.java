package sample;

import java.io.*;
import java.net.*;
import java.util.*;
/**
 * Created by nourhalabi on 27/03/16.
 */
public class ClientConnectionHandler implements Runnable {
    public static String DIR = "SharedFolder";
    private Socket socket = null;
    private BufferedReader requestInput = null;
    private DataOutputStream requestOutput = null;
    FileHandler fh = new FileHandler();

    public ClientConnectionHandler(Socket socket) {
        this.socket = socket;
        try {
            requestInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            requestOutput = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.err.println("Server Error while processing new socket\r\n");
            e.printStackTrace();
        }
    }

    public void run() {
        String mainRequestLine = null;
        try {

//            System.out.println("READ TEST: "+requestInput.read());
  //          System.out.println("READLINE TEST: "+requestInput.readLine());
//            while((mainRequestLine = requestInput.readLine()) != null)
//            {
//               mainRequestLine += requestInput.readLine();
//            }

          //  System.out.println("rainreuestline read in from client:\n" + mainRequestLine);
            mainRequestLine = requestInput.readLine();
            handleRequest(mainRequestLine);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                requestInput.close();
                requestOutput.close();
                socket.close();
            } catch (IOException e2) {}
        }
    }

    private void handleRequest(String mainRequestLine) throws IOException {
        try {

            //System.out.println("file being recived:\n" + mainRequestLine);

            StringTokenizer requestTokenizer = new StringTokenizer(mainRequestLine);
            String command = null;

            command = requestTokenizer.nextToken();
            System.out.println("Command:"+command);
            System.out.println("MainRequestline "+mainRequestLine);

            if(command.equalsIgnoreCase("DIR"))
            {   //return directory list
                sendDirectory();
            }
            else if(command.equalsIgnoreCase("UPLOAD"))
            {
                //get filename token
                String filename = requestTokenizer.nextToken();
                //get contents of the file
                String text = "";

                if (requestTokenizer.hasMoreTokens()) {
                    text = mainRequestLine.substring(mainRequestLine.lastIndexOf(filename) + filename.length(), mainRequestLine.length());
                    System.out.println("FIlename received: " + filename);
                    System.out.println("Text received " + text);
                }

                File newFile = new File ("SharedFolder/" +filename);
                fh.SaveTextToFile(text, newFile);
                sendDirectory();
            }
            else if(command.equalsIgnoreCase("DOWNLOAD"))
            {
                //get filename token
                String filename = requestTokenizer.nextToken();
                //load and send text from textfile


                File textFile = new File ("SharedFolder/" +filename);
                sendFileContents(textFile);
            }
            else
            {
                System.err.println("No such command found for: "+mainRequestLine+"\r\n");
            }

        } catch (NoSuchElementException e) {
            System.err.println("No such command found for: "+mainRequestLine+"\r\n");
            e.printStackTrace();
        }
    }

    private void sendDirectory() throws IOException {
        requestOutput.writeBytes(fh.getDirectoryList(new File(DIR)));
        requestOutput.writeBytes("\r\n");
        requestOutput.flush();
    }


    private void sendFileContents(File textFile) throws IOException {
        requestOutput.writeBytes(fh.ReadtextFromFile(textFile));
        requestOutput.writeBytes("\r\n");
        requestOutput.flush();
    }

}
